import requests
import env
import socket
import time
import threading
import traceback
from datetime import datetime
from queue import Queue, Empty

concurrent = 25
q = Queue(concurrent * 2)

threads = []
config = []
check_timeouts = {}
config_update = None
config_timeout = 60 * 15

VERSION = 1
hostname = socket.gethostname()


def setup():
    global config, config_update

    if config_update is not None and (datetime.now() - config_update).seconds < config_timeout:
        return

    print('UPDATE CONFIG')
    response = requests.get(env.API_HOST + '/api/worker/setup', params={
        'version': VERSION,
        'hostname': hostname
    })
    if response.status_code == 200:
        config = response.json()['data']
        config_update = datetime.now()


def do_work():
    while True:
        try:
            url = q.get(timeout=10)
        except Empty:
            return False

        # print('REQUEST ' + url)
        q.task_done()


def start_thread():
    global threads
    print('START THREAD')
    t = threading.Thread(target=do_work)
    t.daemon = True
    t.start()
    threads.append(t)


def manage_threads():
    for index, thread in enumerate(threads):
        if not thread.is_alive():
            del threads[index]
            print('DELETE THREAD')

    while q.qsize() and len(threads) < concurrent:
        start_thread()


while True:
    try:
        setup()
        for check_session in config:
            key = str(check_session['type'])
            start_checking = datetime.now()

            if key not in check_timeouts or (datetime.now() - check_timeouts[key]).seconds > check_session['timeout']:
                for service in check_session['services']:
                    print('PUT - ' + service)
                    manage_threads()
                    q.put(service)

                check_timeouts[key] = start_checking

            manage_threads()
            time.sleep(1)

        q.join()
    except Exception:
        traceback.print_exc()
        time.sleep(10)
