import requests

bad_phrases = [
    'Hacked by',
    'Viagra',
    'Propecia',
    # 'meta http-equiv="refresh"',
    'write(\'<iframe',
    'write("<iframe',
    'ev​​​al(',
    '<b>Parse error</b>:',
    '<b>Warning</b>:',
]


def verify(url):
    messages = []

    connect_timeout, read_timeout = 5.0, 30.0

    try:
        website_response = requests.get(url, stream=True, timeout=(connect_timeout, read_timeout), headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
        })
        content = website_response.text.lower()
        for bad_phrase in bad_phrases:
            if bad_phrase.lower() in content:
                messages.append('Your website contains `' + bad_phrase + '`.')
    except Exception:
        pass

    try:
        response = requests.post(
            'https://safebrowsing.googleapis.com/v4/threatMatches:find?key=AIzaSyANUIiV8WMBlU4i1jojrHOqrmEaDc4RPBE',
            json={
                "client": {
                    "clientId": url,
                    "clientVersion": "1"
                },
                "threatInfo": {
                    "threatTypes": [
                        'THREAT_TYPE_UNSPECIFIED',
                        'MALWARE',
                        'SOCIAL_ENGINEERING',
                        'UNWANTED_SOFTWARE',
                        'POTENTIALLY_HARMFUL_APPLICATION'
                    ],
                    "platformTypes": ["ANY_PLATFORM"],
                    "threatEntryTypes": ["URL"],
                    "threatEntries": [
                        {"url": url}
                    ]
                }
            }, verify=False).json()

        if response:
            for match in response['matches']:
                messages.append('Your website listed in Google Safe Browsing: ' + match['threatType'] + ' thread')

    except Exception:
        pass

    if messages:
        return "\n".join(messages)
    else:
        return None
