import dns.resolver
import dns.exception
import socket

bls = [
    "zen.spamhaus.org",
    "spam.abuse.ch",
    "cbl.abuseat.org",
    "virbl.dnsbl.bit.nl",
    "dnsbl.inps.de",
    "ix.dnsbl.manitu.net",
    "dnsbl.sorbs.net",
    "bl.spamcannibal.org",
    "bl.spamcop.net",
    "xbl.spamhaus.org",
    "pbl.spamhaus.org",
    "dnsbl-1.uceprotect.net",
    "dnsbl-2.uceprotect.net",
    "dnsbl-3.uceprotect.net",
    "db.wpbl.info",
    'b.barracudacentral.org',
    'dnsbl.sorbs.net',
    'all.spamrats.com',
    'spamsources.fabel.dk',

    #
    # '0spam.fusionzero.com',
    # 'access.redhawk.org',
    # 'all.rbl.jp',
    # 'all.s5h.net',
    # 'all.spamrats.com',
    # 'b.barracudacentral.org',
    # 'bl.blocklist.de',
    # 'bl.emailbasura.org',
    # 'bl.mailspike.org',
    # 'bl.score.senderscore.com',
    # 'bl.spamcannibal.org',
    # 'bl.spamcop.net',
    # 'bl.spameatingmonkey.net',
    # 'bogons.cymru.com',
    # 'cblplus.anti-spam.org.cn',
    # 'db.wpbl.info',
    # 'dnsbl-1.uceprotect.net',
    # 'dnsbl-2.uceprotect.net',
    # 'dnsbl-3.uceprotect.net',
    # 'dnsbl.dronebl.org',
    # 'dnsbl.inps.de',
    # # 'dnsbl.justspam.org',
    # 'dnsbl.kempt.net',
    # 'dnsbl.rv-soft.info',
    # 'dnsbl.sorbs.net',
    # 'dnsbl.tornevall.org',
    # 'dnsbl.webequipped.com',
    # 'dnsrbl.swinog.ch',
    # 'fnrbl.fast.net',
    # 'ips.backscatterer.org',
    # 'ix.dnsbl.manitu.net',
    # 'korea.services.net',
    # 'l2.bbfh.ext.sorbs.net',
    # 'list.blogspambl.com',
    # 'mail-abuse.blacklist.jippg.org',
    # 'psbl.surriel.com',
    # 'rbl2.triumf.ca',
    # 'rbl.dns-servicios.com',
    # 'rbl.efnetrbl.org',
    # 'rbl.polarcomm.net',
    # 'spam.abuse.ch',
    # 'spam.dnsbl.sorbs.net',
    # 'spam.pedantic.org',
    # 'spamguard.leadmon.net',
    # 'spamrbl.imp.ch',
    # 'spamsources.fabel.dk',
    # 'st.technovision.dk',
    # 'tor.dan.me.uk',
    # # 'tor.dnsbl.sectoor.de',
    # 'truncate.gbudb.net',
    # 'ubl.unsubscore.com',
    # 'virbl.dnsbl.bit.nl',
    # 'zen.spamhaus.org',
    # 'rbl.megarbl.net',
    # 'rbl.abuse.ro'
]


def get_mx(domain):
    mx_list = []

    try:
        mxs = dns.resolver.query(domain, 'MX')
    except Exception:
        mxs = []

    for x in mxs:
        mx_list.append(x.exchange.to_text())

    return mx_list


def check(domain):
    bad_results = []
    try:
        ip = socket.gethostbyname(domain)
        clear = False
        for bl in bls:
            status = True
            message = ''
            try:
                my_resolver = dns.resolver.Resolver()
                my_resolver.timeout = 2
                my_resolver.lifetime = 2
                query = '.'.join(reversed(str(ip).split("."))) + "." + bl
                answers = my_resolver.query(query, "A")
                answer_txt = my_resolver.query(query, "TXT")

                message = 'IP: %s IS listed in %s (%s: %s)' % (ip, bl, answers[0], answer_txt[0])
                status = False
                clear = True
            except dns.resolver.NXDOMAIN:
                message = 'NXDOMAIN: ' + bl
                pass
            except dns.exception.Timeout:
                message = 'Timeout: ' + bl
            except dns.resolver.NoAnswer:
                message = 'NoAnswer: ' + bl
            except dns.resolver.NoNameservers:
                message = 'NoNameServers: ' + bl
            except Exception as exc:
                print('Message: ' + str(exc))
                continue
            # print(message)
            if not status:
                bad_results.append(bl + ': ' + message)

        return clear, '\n'.join(bad_results)

    except Exception as exc:
        # print(exc)
        return False, ''
