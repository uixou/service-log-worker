import socket

from celery import Celery
from kombu import Queue, Exchange
import requests
import env
import time
import lib_black_list
import lib_smtp_check
import url_util
import whois
import tldextract
from datetime import datetime, timedelta

def maintenance_service(service):
    domain = url_util.get_domain(service['url'])

    mx_problems = ''


    is_expired = False

    domain_email = ''
    expired_date = ''

    try:
        tld = tldextract.extract(service['url'])
        print(tld)
        if tld.suffix:
            whois_data = whois.whois(tld.domain + '.' + tld.suffix)
            if whois_data.emails:
                domain_email = url_util.get_first(whois_data.emails)

            if whois_data.expiration_date:
                expired_date = url_util.get_first(whois_data.expiration_date) - timedelta(days=7)
                time_threshold = datetime.now()
                if time_threshold < expired_date:
                    is_expired = True
    except whois.parser.PywhoisError as e:
        pass

    print(is_expired, domain_email, expired_date)

    data = {
        'id': service['id'],
        'mx_problems': mx_problems,
        'registrar_email': domain_email,
        'expired_date': expired_date,
        'is_expired': 'yes' if is_expired else 'no',
        'worker': ''
    }



maintenance_service({
    'url': 'https://descarca.net/',
    'id': 0
})