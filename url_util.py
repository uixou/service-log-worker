from urllib.parse import urlparse
import socket


def validate_ip(ip):
    try:
        socket.inet_aton(ip)
        return True
    except socket.error:
        return False


def get_ip(ip):
    if ip and not validate_ip(ip):
        try:
            ip = socket.gethostbyname(ip)
        except:
            ip = None

    return ip if ip else None


def get_domain(url):
    parsed_uri = urlparse(url)

    if parsed_uri.hostname:
        return parsed_uri.hostname.replace('www.', '')

    return None


def get_first(item):
    if isinstance(item, list):
        return item[0]
    else:
        return item
