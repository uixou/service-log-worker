import requests

import math


def analize(domain):
    response = requests.get(
        'https://api.similarweb.com/SimilarWebAddon/' + domain + '/all',
        headers={
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'accept-encoding': 'gzip, deflate, sdch, br',
            'accept-language': 'en-US,en;q=0.8',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
        }
    )

    if response.status_code == 200:

        response_data = response.json()

        if response_data:
            return parse_data(response_data)

        else:
            print('SIMILAR WEB: not found')

    else:
        print('SIMILAR WEB: ' + str(response.status_code) + ' status code')

    return None


def parse_data(data):
    # print(json.dumps(data))

    category = aget(data, 'Category')  #

    result_data = {
        'category': category.replace('_', ' ') if category else None,  #
        'country_id': aget(data, 'CountryRank.Country'),  #
        'engagement_page_per_visits': aget(data, 'Engagments.PagePerVisit'),  #
        'engagement_time_on_site': seconds_to_time(aget(data, 'Engagments.TimeOnSite')),  #
        'engagement_total_visits': math.floor(aget(data, 'Engagments.Visits')),  #
        'global_rank': aget(data, 'GlobalRank.Rank'),  #
        'country_rank': aget(data, 'CountryRank.Rank'),  #
        'category_rank': aget(data, 'CategoryRank.Rank'),  #
        'similar_date': str(aget(data, 'Engagments.Year')) + '-' + str(aget(data, 'Engagments.Month')) + '-01',
        'referrals': [],
        'destinations': [],
        'top_country_shares': [],
        'keywords': [],
    }

    for site in aget(data, 'TopDestinations', []):
        result_data['destinations'].append(aget(site, 'Site'))

    for site in aget(data, 'TopReferring', []):
        result_data['referrals'].append(aget(site, 'Site'))

    for site in aget(data, 'TopCountryShares', []):
        result_data['top_country_shares'].append(aget(site, 'Country'))

    for site in aget(data, 'TopOrganicKeywords', []):
        result_data['keywords'].append(aget(site, 'Keyword'))

    for site in aget(data, 'TopPaidKeywords', []):
        result_data['keywords'].append(aget(site, 'Keyword'))

    return result_data


def aget(obj, path, default_value=None):
    try:
        parts = path.split('.')

        for part in parts:
            if part.isdigit():
                try:
                    obj = obj[int(part)]
                except Exception:
                    obj = obj.get(part)
            else:
                obj = obj.get(part)

        if obj is None:
            return default_value
        else:
            return obj

    except Exception:
        return default_value


def seconds_to_time(seconds):
    if seconds:

        hours = math.floor(seconds / 3600)
        seconds = seconds - hours * 3600
        minutes = math.floor(seconds / 60)
        seconds = math.floor(seconds - minutes * 60)

        return "%02d:%02d:%02d" % (hours, minutes, seconds)
    else:
        return None