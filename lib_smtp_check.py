import smtplib

def verify_server(server_addr):
    localhost = 'hostmonitor.org'
    localhost_email = 'test@' + localhost

    try:
        server = smtplib.SMTP(server_addr, 25, localhost, 15)
        server.ehlo_or_helo_if_needed()

        code, response = server.docmd('MAIL FROM: <' + localhost_email + '>')
        server.quit()

        if code == 250:
            return True, response
        else:
            return False, response

    except Exception as e:
        return False, str(e)


# print(verify_server('alert.hostmonitor.org'))