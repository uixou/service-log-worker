apt-get update

cd /var/
mkdir worker
cd worker

apt-get install python-dev libffi-dev libssl-dev
apt-get install python3-pip
apt-get install git
apt-get install supervisor

git init .
git remote add origin https://bitbucket.org/uixou/service-log-worker.git
git fetch --all
git branch --set-upstream-to master origin/master
git checkout master
git pull

pip3 install virtualenv
virtualenv venv

source venv/bin/activate

pip3 install -r requirements.txt
pip3 install 'requests[security]' --upgrade
pip3 install git+https://github.com/celery/celery.git --upgrade

service supervisor restart
cp servicelog-celery.conf /etc/supervisor/conf.d/apps.conf

supervisorctl reread
supervisorctl update

nano env.py