import socket
import traceback
import lxml.html
import time
from celery import Celery
from kombu import Queue
import requests
import env
import lib_black_list
import lib_smtp_check
import lib_security_check
import lib_similar_stats
import url_util
import whois
import tldextract
import dns.resolver
from datetime import datetime, timedelta
from contextlib import closing

# celery -A tasks worker -l info --concurrency=10

VERSION = 1
hostname = socket.gethostname()

api_response = requests.get(env.API_HOST + '/api/worker/register', params={
    'version': VERSION,
    'hostname': hostname
})

app = Celery(env.BROKER_PATH, broker=env.BROKER)
app.conf.task_queues = (
    Queue('maintenance'),
)
app.conf.enable_utc = False


@app.task(time_limit=120)
def analytics_service(service):
    domain = url_util.get_domain(service['url'])

    active_ports = []
    name_servers = []

    website_keywords = None
    website_description = None
    website_title = None
    try:
        response = requests.get(service['url'], verify=False, timeout=15)

        parsed = lxml.html.fromstring(response.text)

        title_element = parsed.find(".//title")
        if title_element is not None:
            website_title = title_element.text

        description_element = parsed.find('.//meta[@name="description"]')
        if description_element is not None:
            website_description = description_element.get('content')

        keywords_element = parsed.find('.//meta[@name="keywords"]')
        if keywords_element is not None:
            website_keywords = keywords_element.get('content')

    except Exception:
        traceback.print_exc()

    data = {
        'id': service['id'],
        'worker': hostname,
        'similar_web': lib_similar_stats.analize(domain),
        'active_ports': active_ports,
        'name_servers': name_servers,
        'website_description': website_description,
        'website_title': website_title,
        'website_keywords': website_keywords,
    }

    print(data)

    app.send_task('tasks_worker.check_analytics_result', [data], queue='api')


@app.task(time_limit=120)
def maintenance_service(service):
    domain = url_util.get_domain(service['url'])

    domain_b = bytes(domain, encoding='utf-8')
    group_tests = {
        'HTTP': [
            {'port': 80, 'test': 'Make request', 'send': b"GET / HTTP/1.1\r\nHost: " + domain_b + b"\r\n\r\n"},
        ],
        'MAIL': [
            {'port': 25, 'test': 'Simple Mail Transfer Protocol on port 25'},
            {'port': 110, 'test': 'Post Office Protocol (POP3)'},
            {'port': 143, 'test': 'Internet Message Access Protocol (IMAP)'},
            {'port': 465, 'test': 'SMTP SSL on port 465'},
        ],
        'SSH': [
            {'port': 22, 'test': 'SSH'}
        ],
    }

    mx_problems = ''

    socket_done = []

    socket_list = [
        {'group': 'HTTP', 'host': domain},
        {'group': 'SSH', 'host': domain}
    ]

    try:
        answers = dns.resolver.query(domain, 'NS')
        for server in answers:
            dns_server = server.target.to_text()
            socket_list.append({'group': 'DNS', 'host': dns_server})
            start_time = time.time()
            r = dns.resolver.Resolver()
            r.namerservers = [dns_server]
            for r in r.query(domain, 'A'):
                socket_done.append({
                    'group': 'DNS',
                    'host': dns_server,
                    'test': 'Get A records',
                    'time': round(time.time() - start_time, 3),
                    'result': r.to_text()
                })
                break

    except Exception:
        traceback.print_exc()

    is_in_blacklist, bl_message = lib_black_list.check(domain)
    mxs = lib_black_list.get_mx(domain)
    if mxs:
        for mx in mxs:
            start_time = time.time()
            mx_work, mx_work_message = lib_smtp_check.verify_server(mx)
            if not mx_work:
                mx_problems = '\n'.join([mx_problems, mx + ': ' + mx_work_message])
            else:
                socket_done.append({
                    'group': 'MAIL',
                    'host': domain,
                    'test': 'Try sending',
                    'time': round(time.time() - start_time, 3),
                    'result': str(mx_work_message, encoding='utf-8')
                })

            socket_list.append({'group': 'MAIL', 'host': mx})

            mx_is_in_blacklist, mx_message = lib_black_list.check(mx)
            if mx_is_in_blacklist:
                is_in_blacklist = True
                bl_message = '\n'.join([bl_message, mx_message])

    is_expired = False

    domain_email = ''
    expired_date = ''

    try:
        tld = tldextract.extract(service['url'])
        if tld.suffix:
            start_time = time.time()
            whois_data = whois.whois(tld.domain + '.' + tld.suffix)
            if whois_data.emails:
                domain_email = url_util.get_first(whois_data.emails)

            if whois_data.expiration_date:
                socket_done.append({
                    'group': 'WHOIS',
                    'host': domain,
                    'test': 'Get WHOIS Records',
                    'time': round(time.time() - start_time, 3),
                    'result': "Registrar: %s\nWhois server: %s\nCreated: %s\nExpire: %s" % (
                         url_util.get_first(whois_data.registrar),
                         url_util.get_first(whois_data.whois_server),
                         url_util.get_first(whois_data.creation_date),
                         url_util.get_first(whois_data.expiration_date)
                    )
                })

                expired_date = url_util.get_first(whois_data.expiration_date) - timedelta(days=7)
                if datetime.now() > expired_date:
                    is_expired = True
    except whois.parser.PywhoisError as e:
        pass

    security_issues = lib_security_check.verify(service['url'])

    for socket_item in socket_list:
        for group_test in group_tests.get(socket_item['group'], []):
            try:
                start_time = time.time()
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(5)
                s.connect((socket_item['host'], group_test['port']))
                s.send(group_test.get('send', b"Hello, World!"))
                data = str(s.recv(1024), encoding='utf-8')
                s.close()

                if socket_item['group'] =='HTTP':
                    data = url_util.get_first(data.split('\r\n'))

                socket_done.append({
                    'group': socket_item['group'],
                    'host': socket_item['host'],
                    'time': round(time.time() - start_time, 3),
                    'test': group_test['test'],
                    'result': data.strip(),
                })
            except:
                traceback.print_exc()

    data = {
        'id': service['id'],
        'is_in_blacklist': 'yes' if is_in_blacklist else 'no',
        'blacklist_message': bl_message,
        'mx_problems': mx_problems,
        'registrar_email': domain_email,
        'expired_date': expired_date,
        'security_issues': security_issues,
        'service_checks': socket_done,
        'is_expired': 'yes' if is_expired else 'no',
        'worker': hostname
    }

    app.send_task('tasks_worker.check_maintenance_result', [data], queue='api')


# analytics_service({
#     'url': 'http://www.api-marketing.com/',
#     'id': 538
# })

# maintenance_service({
#     'url': 'https://hostmonitor.org/',
#     'id': 83
# })
