import socket

from celery import Celery
from kombu import Queue
import requests
import env
import time
import url_util

# celery -A tasks worker -l info --concurrency=10

VERSION = 1
hostname = socket.gethostname()

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',
    'Accept-Language': 'en-US,en;q=0.9'
}

api_response = requests.get(env.API_HOST + '/api/worker/register', params={
    'version': VERSION,
    'hostname': hostname
})

app = Celery(env.BROKER_PATH, backend='rpc://', broker=env.BROKER)
app.conf.task_queues = (
    Queue('celery'),
    Queue(hostname),
)
app.conf.enable_utc = False
app.conf.timezone = 'UTC'


@app.task(time_limit=30)
def check_service(service):
    http_url = service['url']
    http_code = 0
    http_ssl = False

    up = 0
    ip = None

    http_load_time = time.time()

    connect_timeout, read_timeout = 5.0, 15.0

    try:
        response = requests.get(service['url'], stream=True, timeout=(connect_timeout, read_timeout), headers=headers)
        http_load_time = time.time() - http_load_time

        http_url = response.url
        http_code = response.status_code

        try:
            ip = response.raw._connection.sock.getpeername()
            if ip:
                ip = ip[0]
        except Exception as e:
            pass

        up = 1 if http_code == 200 else 0

        if service['is_https']:
            http_ssl = True

        error_message = response.reason
    except requests.exceptions.SSLError as e:
        http_ssl = False
        http_load_time = 0
        error_message = type(e).__name__
    except Exception as e:
        http_load_time = 0
        up = 0
        error_message = type(e).__name__

    if not ip:
        try:
            ip = url_util.get_ip(url_util.get_domain(service['url']))
        except:
            pass

    result_data = {
        'service_check_id': service['service_check_id'],
        'up': 'up' if up else 'down',
        'ip': ip,
        'up_https': 'up' if http_ssl else 'down',
        'http_url': http_url,
        'http_code': http_code,
        'http_load_time': http_load_time,
        'error_message': error_message,
        'worker': hostname
    }

    app.send_task('tasks_worker.check_service_result', [result_data], queue='api')
